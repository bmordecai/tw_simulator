import telnetlib
import sys
import msvcrt
import time
import logging
from time import sleep

logging.basicConfig(format='%(asctime)s %(levelname)s %(module)s :: %(message)s', level=logging.INFO)


def read_character(prompt=None, timeout=30):
    """
    Reads a single character from the user at the console and gives up waiting for input when the timeout
    expires.

    :param prompt: Text to display to the user.
    :param timeout: How long to wait for user input.

    :return: The key pressed or None
    """

    start_time = time.time()
    if prompt is not None:
        sys.stdout.write(prompt)
    key_input = ''
    while (time.time() - start_time) < timeout and len(key_input) < 1:
        if msvcrt.kbhit():
            key_input = msvcrt.getche()
    if prompt is not None:
        print('')
    if len(key_input) > 0:
        return key_input
    else:
        return None


class TwoWireSimulatorConnection:
    """
    This class handles the communication to the 2-wire simulation.

    """

    log = logging.getLogger(__name__)

    def __init__(self, host=None, port=21000, debug_on=True):
        self.host = host
        self.port = port
        self.tn = telnetlib.Telnet()
        self.debug_on = debug_on

    def open(self):
        """
        Opens the telnet connection to the 2-wire simulation.

        :return:
        """

        self.tn.open(self.host, self.port)

    def close(self):
        """
        Closes the telnet connection to the 2-wire simulation.

        :return:
        """

        self.tn.close()

    def send_recv(self, msg, expected=None):
        """
        Sends a message to the telnet session with the 2-wire simulation.  Then return output based on a
        regular expression, a text string, or just grab what ever comes out quickly.

        :param msg: The text string to send to the 2-wire simulation.
        :param expected: The expected output to wait for.  This can be a string or a regular expression list.

        :return: The output returned by the 2-wire simulation.
        """

        message = msg + '\r'
        self.log.debug("Sent: "+repr(message))
        self.tn.write(message)
        no_out_put = True
        out_put = ""
        while no_out_put:
            if expected is None:
                out_put = self.tn.read_eager()
                if out_put == '':
                    sleep(.2)
                else:
                    self.log.debug("Recd: "+repr(out_put))
                    no_out_put = False
            elif type(expected) == str:
                out_put = self.tn.read_until(expected, 5)
                no_out_put = False
                if out_put == '':
                    self.log.debug("Failed to receive expected : " + repr(expected))
                else:
                    self.log.debug("Recd: "+repr(out_put))
            elif type(expected) == list:
                out_put = self.tn.expect(expected, 5)
                no_out_put = False
                if out_put == '':
                    self.log.debug("Failed to receive expected : " + repr(expected))
                else:
                    self.log.debug("Recd: "+repr(out_put[2]))
            else:
                out_put = "Unknown expect type "+str(expected)
        return out_put
