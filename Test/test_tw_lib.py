from unittest import TestCase
import logging
import time
import tw_lib

logging.basicConfig(format='%(asctime)s %(levelname)s %(module)s %(funcName)s :: %(message)s', level=logging.DEBUG)


class TestTwLib(TestCase):
    log = logging.getLogger(__name__)

    def banner(self, message):
        self.log.debug("*" * 10 + " "+str(message)+" " + "*" * 10)

    def setUp(self):
        self.banner("setUp")

    def tearDown(self):
        self.banner("tearDown")

    def test_read_character(self):
        self.banner("test_read_character")
        timeout_val = 5
        start = time.time()
        ret_val = tw_lib.read_character(prompt=" ? ", timeout=timeout_val)
        stop = time.time()
        self.assertGreaterEqual((stop - start),  timeout_val)
        self.log.debug(ret_val)


class TestTwoWIreSimulatorConnection(TestCase):
    log = logging.getLogger(__name__)

    def banner(self, message):
        self.log.debug("*" * 10 + " "+str(message)+" " + "*" * 10)

    def setUp(self):
        self.banner("setUp")

    def tearDown(self):
        self.banner("tearDown")

    def test_open(self):
        self.banner("test_open")
        start = time.time()
        tw = tw_lib.TwoWireSimulatorConnection(host='localhost', port=999)
        try:
            tw.open()
        except IOError:
            print("Error connection refused as expected.")
        stop = time.time()
        self.assertLessEqual(stop-start, 3)
