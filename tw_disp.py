import re
import tw_lib
import logging

logging.basicConfig(format='%(asctime)s %(levelname)s %(module)s :: %(message)s', level=logging.INFO)


class TwoWireDisplay:
    """
    The 2-wire simulator has a command line interface that is accessible via telnet port.  This utility allows the
    user to monitor active decoders, generate flow on the flow sensors, edit the milliamps reported by the decoders.
    """

    log = logging.getLogger(__name__)

    # Indexes for data in get from simulator
    ENABLE = 1
    ADDRESS = 4
    CURRENT = 8
    STATUS_CODE = 7
    RUNNING = 10
    TOTAL_GALLONS = 10

    def __init__(self, host='localhost', port=21000):
        self.host = host
        self.port = port
        self.conn = tw_lib.TwoWireSimulatorConnection(self.host, self.port)
        self.run_shell = True
        self.stations = {}
        self.pump_stats = {}
        self.master_valves = {}
        self.flow_sensors = {}
        self.min_station = 1
        self.max_stations = 48
        self.time_out = 15
        self.temp_time_out = self.time_out
        self.running_decoders = []
        self.run_flow = False
        self.flow_increment = [10, 10, 10, 10]
        self.flow_decoders = {}

    def update_decoder_info(self, serial, raw_decoder_data):
        """
        Updates the current info on the decoders and store them to the stations dictionary

        :param serial: The serial of the device to update
        :param raw_decoder_data: The tuple returned by the telnetlib with the 2-wire sim response.

        :return:
        """

        self.stations[serial] = {}
        decoder_data = raw_decoder_data[2].split(',')
        if decoder_data[self.ENABLE][-5:].split('=')[1] == 'TR':
            self.stations[serial]['Enabled'] = True
        else:
            self.stations[serial]['Enabled'] = False
        self.stations[serial]['AD'] = int(decoder_data[self.ADDRESS].split('=')[1])
        self.stations[serial]['VA'] = float(decoder_data[self.CURRENT].split('=')[1])
        self.stations[serial]['SS'] = str(decoder_data[self.STATUS_CODE].split('=')[1])
        if decoder_data[self.RUNNING].split('=')[1] == 'TR':
            self.stations[serial]['Running'] = True
            self.running_decoders.append(serial)
        else:
            self.stations[serial]['Running'] = False

    def update_flow_decoder_info(self, serial, raw_decoder_data):
        """
        Updates the current info on the flow decoders and stores them to the flow decoders dictionary

        :param serial: The serial of the device to update
        :param raw_decoder_data: The tuple returned by the telnetlib with the 2-wire sim response.

        :return:
        """

        self.flow_decoders[serial] = {}
        decoder_data = raw_decoder_data[2].split(',')
        if decoder_data[self.ENABLE][-5:].split('=')[1] == 'TR':
            self.flow_decoders[serial]['Enabled'] = True
        else:
            self.flow_decoders[serial]['Enabled'] = False
        self.flow_decoders[serial]['AD'] = int(decoder_data[self.ADDRESS].split('=')[1])
        self.flow_decoders[serial]['VG'] = int(decoder_data[self.TOTAL_GALLONS].split('=')[1])
        self.flow_decoders[serial]['SS'] = str(decoder_data[self.STATUS_CODE].split('=')[1])

    def get_decoder_info(self):
        """
        Requests information from the 2-wire simulator for the decoders stations, flow sensors, master valves, and
        pump starts.  Then it updates the appropriate dictionary.

        :return:
        """

        self.log.info("Getting Sim Decoder Info")
        self.running_decoders = []
        for flow in range(1, 5, 1):
            decoder_serial = "HF%05d" % flow
            sim_to = "get,fm=" + str(decoder_serial)
            sim_return = self.conn.send_recv(sim_to, [re.compile(",FS=TR\r\n"), re.compile(",FS=FA\r\n"), ])
            self.update_flow_decoder_info(decoder_serial, sim_return)
        for decoder_index in range(self.min_station, self.max_stations + 1, 1):
            decoder_serial = "H%06d" % decoder_index
            sim_to = "get,zn=" + str(decoder_serial)
            sim_return = self.conn.send_recv(sim_to, [re.compile(",DL=\\d\r\n"), ])
            self.update_decoder_info(decoder_serial, sim_return)
        for master_valve_index in range(1, 5, 1):
            decoder_serial = "HM%05d" % master_valve_index
            sim_to = "get,zn=" + str(decoder_serial)
            sim_return = self.conn.send_recv(sim_to, [re.compile(",DL=\\d\r\n"), ])
            self.update_decoder_info(decoder_serial, sim_return)
        for pump_start_index in range(1, 5, 1):
            decoder_serial = "HP%05d" % pump_start_index
            sim_to = "get,zn=" + str(decoder_serial)
            sim_return = self.conn.send_recv(sim_to, [re.compile(",DL=\\d\r\n"), ])
            self.update_decoder_info(decoder_serial, sim_return)

    @staticmethod
    def get_decoder_type_by_serial(serial):
        if serial[:2] == 'H0':
            return "Station"
        elif serial[:2] == 'HM':
            return "Master"
        elif serial[:2] == 'HP':
            return "Pump"
        elif serial[:2] == 'FM':
            return "Flow"

    def display_running_decoders(self):
        """
        Displays the active decoders with the Type, Address, System Status, and milliamp setting of that decoder.

        :return:
        """

        milliamp = 0
        if len(self.running_decoders) > 0:
            self.log.info('{:^7}'.format("Type")+"    AD SS  VA")
            for serial in self.running_decoders:
                display = '{:>7}'.format(self.get_decoder_type_by_serial(serial))+" : "
                display += '{:3d}'.format(self.stations[serial]['AD']) + " " + str(self.stations[serial]['SS']) + \
                           '{: 5.3f}'.format(self.stations[serial]['VA']) + " "
                self.log.info(display)
                milliamp += self.stations[serial]['VA']
            self.log.info("Total mA: "+str('{: 5.3f}'.format(milliamp)))
        else:
            self.log.info("No Running Decoders")

    def option_display_flow(self):
        """
        Displays the current flow setting for the VG Total Gallons increment.

        :return:
        """

        for index in range(1, 5, 1):
            decoder_serial = "HF%05d" % index
            print(str(self.flow_decoders[decoder_serial])+" VG Increment: "+str(self.flow_increment[index-1]))

    def display_options(self):
        print("(1) - Change Update Frequency = "+str(self.time_out)+" seconds.")
        print("(2) - Change decoder(s) milliamp setting.")
        print("(3) - Change decoder(s) status setting.")
        print("(4) - Display decoder(s) info.")
        print("(5) - Enable/Disable Flow.")
        print("(6) - Edit Flow Settings.")
        print("(7) - Display Flow")
        print("(q) - Quit.")

    def pick_options(self):
        response = raw_input('?')
        if response == 'q':
            self.option_exit()
        elif response == '1':
            self.option_change_update_frequency()
        elif response == '2':
            self.option_change_decoder_milliamp()
        elif response == '3':
            self.option_change_decoder_status()
        elif response == '4':
            self.option_display_decoder_info()
        elif response == '5':
            self.option_enable_disable_flow()
        elif response == '6':
            self.option_flow_edit_settings()
        elif response == '7':
            self.option_display_flow()

    def option_flow_edit_settings(self):
        """
        Allows user to change the flow increment for each flow decoder.

        :return:
        """

        start, end = self.pick_decoder_range("1-4")
        print("New Flow Increment ? ")
        new_flow_increment = int(raw_input())
        for index in range(start - 1, end, 1):
            self.flow_increment[index] = new_flow_increment

    def update_flow_decoders(self):
        """
        Increments the VG Total Gallons Usage Increment to produce flow on the controller.

        :return:
        """

        for index in range(1, 5, 1):
            decoder_serial = "HF%05d" % index
            sim_out = "set,fm=" + str(decoder_serial) + ",VG=" + str(self.flow_increment[index-1] +
                                                                     self.flow_decoders[decoder_serial]['VG'])
            self.conn.send_recv(sim_out, [re.compile("OK\r\n"), ])

    def option_enable_disable_flow(self):
        """
        Enables or disables flow and sets the time out to 10 seconds when flow is enabled.   This setting makes sure
        the flow increment is done at the correct interval on the simulator.

        :return:
        """

        if self.run_flow:
            self.run_flow = False
            self.time_out = self.temp_time_out
        else:
            self.run_flow = True
            self.temp_time_out = self.time_out
            self.time_out = 10

    def option_exit(self):
        """
        Quit running this script.

        :return:
        """

        self.run_shell = False

    def option_change_update_frequency(self):
        """
        Set how often the data is updated and the flow is incremented if the flow is enabled.

        :return:
        """

        response = raw_input('New Frequency ? ')
        self.time_out = int(response)

    @staticmethod
    def pick_milliamp():
        print("Milliamp ? ")
        return float(raw_input())

    def option_change_decoder_milliamp(self):
        """
        Set the milliamp in the simulation for the decoder return value for the solenoid.

        :return:
        """

        print("(1) - Change Station Milliamp")
        print("(2) - Change Master Valve Milliamp")
        print("(3) - Change Pumpstart Milliamp")
        response = raw_input('?')
        if response == '1':
            start, end = self.pick_decoder_range(str(self.min_station) + "-" + str(self.max_stations))
            self.set_decoder("H0", start, end, "VA", self.pick_milliamp())
        elif response == '2':
            start, end = self.pick_decoder_range("1-4")
            self.set_decoder("HM", start, end, "VA", self.pick_milliamp())
        elif response == '3':
            start, end = self.pick_decoder_range("1-4")
            self.set_decoder("HP", start, end, "VA", self.pick_milliamp())

    def option_change_decoder_status(self):
        """
        Change the simulated decoder SS System Status.

        :return:
        """

        system_status = {
            1: 'OK',
            2: 'NR',
            3: 'CS',
            4: 'OC',
            5: 'OP',
            6: 'LV'
        }
        start, end = self.pick_decoder_range(str(self.min_station) + "-" + str(self.max_stations))
        print("(1) Status = OK (Device is OK)")
        print("(2) Status = NR (Device no response)          ??")
        print("(3) Status = CS (Device checksum error)       ??")
        print("(4) Status = OC (Solenoid Over Current Error) OC")
        print("(5) Status = OP (Solenoid Open Circuit Error) NC")
        print("(6) Status = LV (Solenoid Low Voltage Error)  ??")
        choice = int(raw_input(" ? "))
        self.set_decoder("H0", start, end, "SS", system_status[choice])

    def option_display_decoder_info(self):
        """
        Display the most recent decoder information.

        :return:
        """

        start, end = self.pick_decoder_range(str(self.min_station) + "-" + str(self.max_stations))
        self.get_decoder_info()
        for index in range(start, end + 1, 1):
            decoder_serial = "H%06d" % index
            self.log.info(self.stations[decoder_serial])

    @staticmethod
    def pick_decoder_range(decoder_range):
        """
        Parses the user input to determine the single decoder or range of decoders.  The user response should be a
        single number or a range of numbers with '-' in the middle.

        :param decoder_range: The string a user entered.
        :return:
        """

        print("Decoder(s) " + str(decoder_range) + " ? ")
        response = raw_input()
        response_split = response.split('-')
        if len(response_split) > 1:
            return int(response_split[0]), int(response_split[1])
        else:
            return int(response_split[0]), int(response_split[0])

    def set_decoder(self, prefix_serial, start, end, option, value):
        """
        Set a value for a range of decoders.

        :param prefix_serial: H0 for station, HM for master valve, HP for pumpstart, HF for flow sensor.
        :param start: The first decoder in a range.
        :param end: The last decoder in a range
        :param option: The parameter to change on the simulated decoder.
        :param value: The value to set the parameter to.
        :return:
        """

        for decoder_index in range(start, end + 1, 1):
            decoder_serial = str(prefix_serial)+"%05d" % decoder_index
            out_put = self.conn.send_recv("set,zn="+str(decoder_serial)+","+str(option)+"="+str(value),
                                          [re.compile("\r\nOK\r\n"), ])
            print(repr(out_put[2]))

    def prompt(self):
        """
        The prompt to the user every time the decoder information display is updated.

        :return:
        """

        if self.run_flow:
            prompt = "Flow Enabled : "
        else:
            prompt = "Flow Disabled : "
        prompt += " Press (e) for menu.  Any other key will refresh decoder status."
        key_input = tw_lib.read_character(prompt, timeout=self.time_out)
        if key_input is None:
            self.run_shell = True
        elif key_input == 'e':
            self.display_options()
            self.pick_options()

    def main(self):
        """
        The main loop of the program.

        :return:
        """

        self.conn.open()
        while self.run_shell:
            self.get_decoder_info()
            self.display_running_decoders()
            if self.run_flow:
                self.update_flow_decoders()
            self.prompt()
        self.conn.close()


if __name__ == '__main__':
    tw = TwoWireDisplay()
    tw.main()
