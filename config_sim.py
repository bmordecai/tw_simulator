import re
from tw_lib import TwoWireSimulatorConnection

conn = TwoWireSimulatorConnection('localhost', 21000)
conn.open()
max_stations = 48
out_string = "do,cl=al"
re_ok = [re.compile("OK\r\n"), ]
re_get = [re.compile(",DL=\\d\r\n"), ]
flow_get = [re.compile(",FS=TR\r\n"), re.compile(",FS=FA\r\n"), ]
conn.send_recv(out_string, re_ok)
for flow in range(1, 5, 1):
    device_serial = "HF%05d" % flow
    out_string = "dev,fm="+str(device_serial)
    conn.send_recv(out_string, re_ok)
    out_string = "set,fm="+str(device_serial)+",AD="+str(flow+212)
    conn.send_recv(out_string, re_ok)
    out_string = "get,fm="+str(device_serial)
    print(conn.send_recv(out_string, flow_get))
for master in range(1, 5, 1):
    device_serial = "HM%05d" % master
    out_string = "dev,D1="+str(device_serial)
    conn.send_recv(out_string, re_ok)
    out_string = "set,zn="+str(device_serial)+",AD="+str(master+200)
    conn.send_recv(out_string, re_ok)
    out_string = "get,zn="+str(device_serial)
    print(conn.send_recv(out_string, re_get))
for pump in range(1, 5, 1):
    device_serial = "HP%05d" % pump
    out_string = "dev,D1="+str(device_serial)
    conn.send_recv(out_string, re_ok)
    out_string = "set,zn="+str(device_serial)+",AD="+str(pump+206)
    conn.send_recv(out_string, re_ok)
    out_string = "get,zn="+str(device_serial)
    print(conn.send_recv(out_string, re_get))
for serial in range(1, max_stations + 1, 1):
    device_serial = "H%06d" % serial
    out_string = "dev,D1="+str(device_serial)
    conn.send_recv(out_string, re_ok)
    out_string = "set,zn="+str(device_serial)+",AD="+str(serial)
    conn.send_recv(out_string, re_ok)
    out_string = "get,zn="+str(device_serial)
    print(conn.send_recv(out_string, re_get))
conn.send_recv("do,bk=usb", re_ok)
conn.close()
