import re
import tw_lib

flow_increment = 100
flow_timer_tic = 10
run_it = True
conn = tw_lib.TwoWireSimulatorConnection('localhost', 21000)
conn.open()
out_string = "do,cl=al"
re_ok = [re.compile("OK\r\n"), ]
re_get = [re.compile(",DL=\\d\r\n"), ]
flow_get = [re.compile(",FS=TR\r\n"), re.compile(",FS=FA\r\n"), ]
conn.send_recv(out_string, re_ok)
for flow in range(1, 5, 1):
    device_serial = "HF%05d" % flow
    out_string = "dev,fm="+str(device_serial)
    conn.send_recv(out_string, re_ok)
    out_string = "set,fm="+str(device_serial)+",AD="+str(flow+212)
    conn.send_recv(out_string, re_ok)
    out_string = "get,fm="+str(device_serial)
    print(conn.send_recv(out_string, flow_get))
while run_it:
    for flow in range(1, 5, 1):
        device_serial = "HF%05d" % flow
        out_string = "get,fm="+str(device_serial)
        ret_val = conn.send_recv(out_string, flow_get)
        sensor = ret_val[2].split(',')
        vg_val = sensor[10].split('=')[1]
        new_vg = int(vg_val) + flow_increment
        out_string = "set,fm="+str(device_serial)+",VG="+str(new_vg)
        conn.send_recv(out_string, re_ok)
    print("Press 's' to stop.")
    if tw_lib.read_character(timeout=flow_timer_tic) == 's':
        run_it = False
conn.close()
